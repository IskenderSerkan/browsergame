<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidesTable extends Migration
{
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->id();
            $table->string('desktop_img');
            $table->string('mobile_img');
            $table->string('link')->nullable();
            $table->tinyInteger('order');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('slides_langs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('slide_id');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('button', 50)->nullable();
            //Dil tablosu ilişkisi için
            $table->string('locale', 5)->index();
            $table->unique(['slide_id', 'locale']);
            $table->foreign('slide_id')->references('id')->on('slides')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('slides_langs');
        Schema::dropIfExists('slides');
    }
}
