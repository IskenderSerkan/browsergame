<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('order')->default(0);
            $table->boolean('footer');
            $table->boolean('submenu');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('pages_langs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('page_id');
            $table->string('title');
            $table->text('content');
            //Dil tablosu ilişkisi için
            $table->string('locale', 5)->index();
            $table->unique(['page_id', 'locale']);
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('pages_langs');
        Schema::dropIfExists('pages');
    }
}
