<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('image')->nullable();
            $table->tinyInteger('order')->default(0);
            $table->timestamps();
            $table->boolean('active')->nullable();
        });

        Schema::create('categories_langs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id'); // Tablo ilişkisi kurulacaksa unsigned (yalnızca pozitif değer) olması şart
            $table->string('name', 200);
            $table->text('description');
            //Dil tablosu ilişkisi için
            $table->string('locale', 5)->index();
            $table->unique(['category_id', 'locale']);
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('categories_langs');
        Schema::dropIfExists('categories');
    }
}
