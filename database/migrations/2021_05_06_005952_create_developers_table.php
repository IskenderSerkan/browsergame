<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevelopersTable extends Migration
{
    public function up()
    {
        Schema::create('developers', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('logo')->nullable();
            $table->tinyInteger('order')->default(0);
            $table->timestamps();
            $table->boolean('active')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('developers');
    }
}
