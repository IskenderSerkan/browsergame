<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('poster');
            $table->string('embed');
            $table->unsignedBigInteger('developer_id')->nullable();
            $table->boolean('mobile');
            $table->integer('click');
            $table->date('release_date')->nullable();
            $table->boolean('active');
            $table->unsignedBigInteger('adding_by');
            $table->timestamps();
            $table->foreign('developer_id')->references('id')->on('developers')->onDelete('cascade');
            $table->foreign('adding_by')->references('id')->on('users');
        });

        Schema::create('games_langs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('game_id');
            $table->string('name', 200);
            $table->text('description');
            //Dil tablosu ilişkisi için
            $table->string('locale', 5)->index();
            $table->unique(['game_id', 'locale']);
            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('games_langs');
        Schema::dropIfExists('games');
    }
}
