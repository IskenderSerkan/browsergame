@extends('admin.inc.master')
@section('pagename', 'Oyunlar')

@section('meta')
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/datatables/datatables.min.css') }}">
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="listtable" class="table table-bordered table-striped datatable" style="width:100%" data-ajax="{{ route('game.index') }}">
                                <thead>
                                    <tr class="filter-area">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>
                                            <select class="form-control table-filter">
                                                <option value="">Tümü</option>
                                                @foreach ($filter['categories'] as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th>
                                            <select class="form-control table-filter">
                                                <option value="">Tümü</option>
                                                @foreach ($filter['developers'] as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th>
                                            <select class="form-control table-filter">
                                                <option value="">Tümü</option>
                                                @foreach ($filter['admins'] as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach

                                            </select>
                                        </th>
                                        <th>
                                            <select class="form-control table-filter">
                                                <option value="">Tümü</option>
                                                @foreach ($filter['active'] as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th></th>
                                    </tr>
                                    <tr class="table-head">
                                        <th data-data="id" data-width="50">ID</th>
                                        <th data-data="poster" data-orderable="false" data-searchable="false" class="dt-center" data-width="150" data-class="dt-center">Poster</th>
                                        <th data-data="name">Oyun Adı</th>
                                        <th data-data="categories" data-width="150">Kategori</th>
                                        <th data-data="developer" data-width="150">Geliştirici</th>
                                        <th data-data="addingby" data-width="150">Ekleyen</th>
                                        <th data-data="active" data-width="100">Durumu</th>
                                        <th data-data="action" data-orderable="false" data-searchable="false" data-width="100">İşlem</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/admin/plugins/datatables/datatables.min.js') }}"></script>
@endsection
