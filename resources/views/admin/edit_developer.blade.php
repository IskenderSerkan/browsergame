@extends('admin.inc.master')
@section('pagename', empty($dataDeveloper) ? 'Geliştirici Ekle' : 'Geliştirici Düzenle')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{ isset($dataDeveloper->id) ? route('developer.update', $dataDeveloper->id) : route('developer.store') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            {{-- Düzenleme ise post yerine patch metodu uygulanır --}}
                            @if (isset($dataDeveloper->id))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Geliştirici Adı</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Geliştirici Adı" maxlength="200" value="{{ isset($dataDeveloper->name) ? $dataDeveloper->name : old('name') }}" required>
                                        @error('name')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="logo" class="col-sm-2 col-form-label">Logosu</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="logo" name="logo" onchange="previewImage(this)" accept="image/*">
                                                <label class="custom-file-label" for="logo">Görsel Seç</label>
                                            </div>
                                        </div>
                                        @error('logo')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror

                                        <label for="logo">
                                            <img class="preview-img" style="width: 300px;height:300px" src="{{ isset($dataDeveloper->logo) ? url('uploads/developer/' . $dataDeveloper->logo) : '#' }}" alt="image">
                                        </label>
                                    </div>
                                    <div class="col-sm-5">
                                        <p class="helpline">
                                            300x300px ebatlarında görsel yüklenmelidir. (Geçersiz ölçüler otomatik ebatlanır)
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="order" class="col-sm-2 col-form-label">Sıra No</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" id="order" name="order" value="{{ isset($dataDeveloper->order) ? $dataDeveloper->order : (old('order') ? old('order') : 0) }}" required>
                                        @error('order')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="active" class="col-sm-2 col-form-label">Aktif</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" class="custom-control-input" id="active" name="active" value="1" {{ isset($dataDeveloper) && isset($dataDeveloper->getAttributes()['active']) ? ($dataDeveloper->getAttributes()['active'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="active">Aktif</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary float-right">Kaydet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/admin/plugins/bs-custom-file-input.js') }}"></script>
@endsection
