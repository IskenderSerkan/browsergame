@extends('admin.inc.master')
@section('pagename', 'Slaytlar')

@section('meta')
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/datatables/datatables.min.css') }}">
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="listtable" class="table table-bordered table-striped datatable" style="width:100%" data-ajax="{{ route('slide.index') }}">
                                <thead>
                                    <tr class="filter-area">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>
                                            <select class="form-control table-filter">
                                                <option value="">Tümü</option>
                                                @foreach ($filter['active'] as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th></th>
                                    </tr>
                                    <tr class="table-head">
                                        <th data-data="id" data-width="50">ID</th>
                                        <th data-data="title">Başlık</th>
                                        <th data-data="desktop_img" class="dt-center" data-width="150" data-class="dt-center">Görsel</th>
                                        <th data-data="active" data-width="150">Durumu</th>
                                        <th data-data="action" data-orderable="false" data-searchable="false" data-width="100">İşlem</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/admin/plugins/datatables/datatables.min.js') }}"></script>
@endsection
