@extends('admin.inc.master')
@section('pagename', empty(@$dataSlide) ? 'Slayt Ekle' : 'Slayt Düzenle')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <form class="form-horizontal" action="{{ isset($dataSlide->id) ? route('slide.update', $dataSlide->id) : route('slide.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- Düzenleme ise post yerine patch metodu uygulanır --}}
                @if (isset($dataSlide->id))
                    @method('PUT')
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="desktop_img" class="col-sm-2 col-form-label">Masaüstü Görsel</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="desktop_img" name="desktop_img" onchange="previewImage(this)" accept="image/*">
                                                <label class="custom-file-label" for="desktop_img">Görsel Seç</label>
                                            </div>
                                        </div>
                                        @error('desktop_img')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror

                                        <label for="desktop_img">
                                            <img class="preview-img" style="width: 400px;height:207px" src="{{ isset($dataSlide->desktop_img) ? url('uploads/slide/' . $dataSlide->desktop_img) : '#' }}" alt="image">
                                        </label>
                                    </div>
                                    <div class="col-sm-5">
                                        <p class="helpline">
                                            1160x600px ebatlarında görsel yüklenmelidir. (Geçersiz ölçüler otomatik ebatlanır)
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="mobile_img" class="col-sm-2 col-form-label">Mobil Görsel</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="mobile_img" name="mobile_img" onchange="previewImage(this)" accept="image/*">
                                                <label class="custom-file-label" for="mobile_img">Görsel Seç</label>
                                            </div>
                                        </div>
                                        @error('mobile_img')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror

                                        <label for="mobile_img">
                                            <img class="preview-img" style="width: 350px;height:311px" src="{{ isset($dataSlide->mobile_img) ? url('uploads/slide/' . $dataSlide->mobile_img) : '#' }}" alt="image">
                                        </label>
                                    </div>
                                    <div class="col-sm-5">
                                        <p class="helpline">
                                            450x400px ebatlarında görsel yüklenmelidir. (Geçersiz ölçüler otomatik ebatlanır)
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="link" class="col-sm-2 col-form-label">Bağlantı adresi</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="link" name="link" value="{{ isset($dataSlide->link) ? $dataSlide->link : old('link') }}" required>
                                        @error('link')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="order" class="col-sm-2 col-form-label">Sıra No</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" id="order" name="order" value="{{ isset($dataSlide->order) ? $dataSlide->order : 0 }}" required>
                                        @error('order')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="active" class="col-sm-2 col-form-label">Aktif</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" class="custom-control-input" id="active" name="active" value="1" {{ isset($dataSlide) && isset($dataSlide->getAttributes()['active']) ? ($dataSlide->getAttributes()['active'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="active">Aktif</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="langtabs" role="tablist">
                                    @foreach ($langs as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="{{ $key }}" data-toggle="pill" href="#{{ $key }}-tab" role="tab" aria-controls="custom-tabs-one-home">
                                                <div class="custom-control custom-switch langselect">
                                                    <input type="checkbox" class="custom-control-input" id="langcheck{{ $key }}" {{ isset($dataSlide) && $dataSlide->translate($key) !== null ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="langcheck{{ $key }}"></label>
                                                </div>
                                                {{ $lang }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="langcontents">
                                    @foreach ($langs as $key => $lang)
                                        @php
                                            $data = isset($dataSlide) && $dataSlide->translate($key) !== null ? $dataSlide->translate($key) : null;
                                        @endphp
                                        <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="{{ $key }}-tab" role="tabpanel">
                                            <div class="form-group row">
                                                <label for="title-{{ $key }}" class="col-sm-2 col-form-label">Başlık Metni</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" id="title-{{ $key }}" name="{{ $key }}[title]" maxlength="250" value="{{ @$data->title }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="description-{{ $key }}" class="col-sm-2 col-form-label">Metin</label>
                                                <div class="col-sm-5">
                                                    <textarea class="form-control" name="{{ $key }}[description]" id="description-{{ $key }}" maxlength="250" cols="30" rows="10">{{ @$data->description }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="button-{{ $key }}" class="col-sm-2 col-form-label">Buton Metni</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" id="button-{{ $key }}" name="{{ $key }}[button]" maxlength="50" value="{{ @$data->button }}">
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Kaydet</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/admin/plugins/bs-custom-file-input.js') }}"></script>
@endsection
