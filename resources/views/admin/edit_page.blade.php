@extends('admin.inc.master')
@section('pagename', empty(@$dataPage) ? 'Sayfa Ekle' : 'Sayfa Düzenle')

@section('meta')
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.css') }}">
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <form class="form-horizontal" action="{{ isset($dataPage->id) ? route('page.update', $dataPage->id) : route('page.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- Düzenleme ise post yerine patch metodu uygulanır --}}
                @if (isset($dataPage->id))
                    @method('PUT')
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="order" class="col-sm-2 col-form-label">Sıra No</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" id="order" name="order" value="{{ isset($dataPage->order) ? $dataPage->order : 0 }}" required>
                                        @error('order')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="footer" class="col-sm-2 col-form-label">Site Altında Göster</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="footer" value="0">
                                            <input type="checkbox" class="custom-control-input" id="footer" name="footer" value="1" {{ isset($dataPage) && isset($dataPage->getAttributes()['footer']) ? ($dataPage->getAttributes()['footer'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="footer">Site Alt Menüde görünsün</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="submenu" class="col-sm-2 col-form-label">Yan Menüde Göster</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="submenu" value="0">
                                            <input type="checkbox" class="custom-control-input" id="submenu" name="submenu" value="1" {{ isset($dataPage) && isset($dataPage->getAttributes()['submenu']) ? ($dataPage->getAttributes()['submenu'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="submenu">Sayfalar yan menüsünden gürünsün</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="active" class="col-sm-2 col-form-label">Aktif</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" class="custom-control-input" id="active" name="active" value="1" {{ isset($dataPage) && isset($dataPage->getAttributes()['active']) ? ($dataPage->getAttributes()['active'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="active">Aktif</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="langtabs" role="tablist">
                                    @foreach ($langs as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="{{ $key }}" data-toggle="pill" href="#{{ $key }}-tab" role="tab" aria-controls="custom-tabs-one-home">
                                                <div class="custom-control custom-switch langselect">
                                                    <input type="checkbox" class="custom-control-input" id="langcheck{{ $key }}" {{ isset($dataPage) && $dataPage->translate($key) !== null ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="langcheck{{ $key }}"></label>
                                                </div>
                                                {{ $lang }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="langcontents">
                                    @foreach ($langs as $key => $lang)
                                        @php
                                            $data = isset($dataPage) && $dataPage->translate($key) !== null ? $dataPage->translate($key) : null;
                                        @endphp
                                        <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="{{ $key }}-tab" role="tabpanel">
                                            <div class="form-group row">
                                                <label for="title-{{ $key }}" class="col-sm-2 col-form-label">Başlık</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" id="title-{{ $key }}" name="{{ $key }}[title]" maxlength="250" value="{{ @$data->title }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="content-{{ $key }}" class="col-sm-2 col-form-label">İçerik</label>
                                                <div class="col-sm-5">
                                                    <textarea class="form-control summernote" name="{{ $key }}[content]" id="content-{{ $key }}" cols="30" rows="10">{{ @$data->content }}</textarea>
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Kaydet</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/summernote/lang/summernote-tr-TR.min.js') }}"></script>
@endsection
