@extends('admin.inc.master')
@section('pagename', empty(@$dataGame) ? 'Oyun Ekle' : 'Oyun Düzenle')

@section('meta')
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.css') }}">
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <form class="form-horizontal" action="{{ isset($dataGame->id) ? route('game.update', $dataGame->id) : route('game.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- Düzenleme ise post yerine patch metodu uygulanır --}}
                @if (isset($dataGame->id))
                    @method('PUT')
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="poster" class="col-sm-2 col-form-label">Oyun Görseli</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="poster" name="poster" onchange="previewImage(this)" accept="image/*">
                                                <label class="custom-file-label" for="poster">Görsel Seç</label>
                                            </div>
                                        </div>
                                        @error('poster')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror

                                        <label for="poster">
                                            <img class="preview-img" style="width: 240px;height:340px" src="{{ isset($dataGame->poster) ? url('uploads/game/' . $dataGame->poster) : '#' }}" alt="image">
                                        </label>
                                    </div>
                                    <div class="col-sm-5">
                                        <p class="helpline">
                                            240x340px ebatlarında görsel yüklenmelidir. (Geçersiz ölçüler otomatik ebatlanır)
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="embed" class="col-sm-2 col-form-label">Embed Kod</label>
                                    <div class="col-sm-5">
                                        <textarea name="embed" id="embed" class="form-control" cols="10" rows="5">{{ isset($dataGame->embed) ? $dataGame->embed : old('embed') }}</textarea>
                                        @error('embed')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="categories" class="col-sm-2 col-form-label">Kategori</label>
                                    <div class="col-sm-5">
                                        <select class="form-control select2" style="width: 100%" id="categories" name="categories[]" data-placeholder="Kategori Seçin" multiple="multiple">
                                            @foreach ($categories as $key => $category)
                                                <option value="{{ $category->id }}" {{ isset($selectedCategories) && in_array($category->id, $selectedCategories) ? 'selected' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('categories')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="developer_id" class="col-sm-2 col-form-label">Geliştirici</label>
                                    <div class="col-sm-5">
                                        <select class="form-control select2" style="width: 100%" id="developer_id" name="developer_id" data-placeholder="Geliştirici Seçin" data-allow-clear="true">
                                            <option></option>
                                            @foreach ($developers as $key => $developer)
                                                <option value="{{ $key }}" {{ @$dataGame->developer_id == $key ? 'selected' : '' }}>{{ $developer }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="mobile" class="col-sm-2 col-form-label">Mobil Destekli</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="mobile" value="0">
                                            <input type="checkbox" class="custom-control-input" id="mobile" name="mobile" value="1" {{ isset($dataGame) && @$dataGame->getAttributes()['mobile'] == 1 ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="mobile">Mobil Destekli</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="click" class="col-sm-2 col-form-label">Tıklanma</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" id="click" name="click" value="{{ isset($dataGame->click) ? $dataGame->click : (old('click') ? old('click') : 0) }}" required>
                                        @error('click')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="release_date" class="col-sm-2 col-form-label">Çıkış Tarihi</label>
                                    <div class="col-sm-5">
                                        <input type="date" class="form-control" id="release_date" name="release_date" value="{{ isset($dataGame->release_date) ? $dataGame->release_date : (old('release_date') ? old('release_date') : '') }}" required>
                                        @error('click')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="active" class="col-sm-2 col-form-label">Aktif</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" class="custom-control-input" id="active" name="active" value="1" {{ isset($dataGame) && isset($dataGame->getAttributes()['active']) ? ($dataGame->getAttributes()['active'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="active">Aktif</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="langtabs" role="tablist">
                                    @foreach ($langs as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="{{ $key }}" data-toggle="pill" href="#{{ $key }}-tab" role="tab" aria-controls="custom-tabs-one-home">
                                                <div class="custom-control custom-switch langselect">
                                                    <input type="checkbox" class="custom-control-input" id="langcheck{{ $key }}" {{ isset($dataGame) && $dataGame->translate($key) !== null ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="langcheck{{ $key }}"></label>
                                                </div>
                                                {{ $lang }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="langcontents">
                                    @foreach ($langs as $key => $lang)
                                        @php
                                            $data = isset($dataGame) && $dataGame->translate($key) !== null ? $dataGame->translate($key) : null;
                                        @endphp
                                        <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="{{ $key }}-tab" role="tabpanel">
                                            <div class="form-group row">
                                                <label for="name-{{ $key }}" class="col-sm-2 col-form-label">Oyun Adı</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" id="name-{{ $key }}" name="{{ $key }}[name]" maxlength="200" value="{{ @$data->name }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="description-{{ $key }}" class="col-sm-2 col-form-label">Oyun Açıklaması</label>
                                                <div class="col-sm-5">
                                                    <textarea class="form-control summernote" name="{{ $key }}[description]" id="description-{{ $key }}" cols="30" rows="10">{{ @$data->description }}</textarea>


                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Kaydet</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/admin/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/select2/i18n/tr.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/summernote/lang/summernote-tr-TR.min.js') }}"></script>
@endsection
