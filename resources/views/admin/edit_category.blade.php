@extends('admin.inc.master')
@section('pagename', empty(@$dataCategory) ? 'Kategori Ekle' : 'Kategori Düzenle')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <form class="form-horizontal" action="{{ isset($dataCategory->id) ? route('category.update', $dataCategory->id) : route('category.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- Düzenleme ise post yerine patch metodu uygulanır --}}
                @if (isset($dataCategory->id))
                    @method('PUT')
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="image" class="col-sm-2 col-form-label">Kategori Görseli</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="image" name="image" onchange="previewImage(this)" accept="image/*">
                                                <label class="custom-file-label" for="image">Görsel Seç</label>
                                            </div>
                                        </div>
                                        @error('image')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror

                                        <label for="image">
                                            <img class="preview-img" style="width: 200px;height:200px" src="{{ isset($dataCategory->image) ? url('uploads/category/' . $dataCategory->image) : '#' }}" alt="image">
                                        </label>
                                    </div>
                                    <div class="col-sm-5">
                                        <p class="helpline">
                                            400x400px ebatlarında görsel yüklenmelidir. (Geçersiz ölçüler otomatik ebatlanır)
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="order" class="col-sm-2 col-form-label">Sıra No</label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control" id="order" name="order" value="{{ isset($dataCategory->order) ? $dataCategory->order : 0 }}" required>
                                        @error('order')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="active" class="col-sm-2 col-form-label">Aktif</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" class="custom-control-input" id="active" name="active" value="1" {{ isset($dataCategory) && isset($dataCategory->getAttributes()['active']) ? ($dataCategory->getAttributes()['active'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="active">Aktif</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="langtabs" role="tablist">
                                    @foreach ($langs as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $loop->first ? 'active' : '' }}" id="{{ $key }}" data-toggle="pill" href="#{{ $key }}-tab" role="tab" aria-controls="custom-tabs-one-home">
                                                <div class="custom-control custom-switch langselect">
                                                    <input type="checkbox" class="custom-control-input" id="langcheck{{ $key }}" {{ isset($dataCategory) && $dataCategory->translate($key) !== null ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="langcheck{{ $key }}"></label>
                                                </div>
                                                {{ $lang }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="langcontents">
                                    @foreach ($langs as $key => $lang)
                                        @php
                                            $data = isset($dataCategory) && $dataCategory->translate($key) !== null ? $dataCategory->translate($key) : null;
                                        @endphp
                                        <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="{{ $key }}-tab" role="tabpanel">
                                            <div class="form-group row">
                                                <label for="name-{{ $key }}" class="col-sm-2 col-form-label">Kategori Adı</label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" id="name-{{ $key }}" name="{{ $key }}[name]" maxlength="200" value="{{ @$data->name }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="description-{{ $key }}" class="col-sm-2 col-form-label">Kategori Açıklaması</label>
                                                <div class="col-sm-5">
                                                    <textarea class="form-control" name="{{ $key }}[description]" id="description-{{ $key }}" cols="30" rows="10">{{ @$data->description }}</textarea>
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Kaydet</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/admin/plugins/bs-custom-file-input.js') }}"></script>
@endsection
