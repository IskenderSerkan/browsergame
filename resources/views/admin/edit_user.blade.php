@extends('admin.inc.master')
@section('pagename', empty($dataUser) ? 'Kullanıcı Ekle' : 'Kullanıcı Düzenle')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form class="form-horizontal" action="{{ isset($dataUser->id) ? route('user.update', $dataUser->id) : route('user.store') }}" method="POST">
                            @csrf
                            {{-- Düzenleme ise post yerine patch metodu uygulanır --}}
                            @if (isset($dataUser->id))
                                @method('PUT')
                            @endif
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Ad Soyad</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Ad Soyad" maxlength="200" value="{{ isset($dataUser->name) ? $dataUser->name : old('name') }}" required>
                                        @error('name')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">E-Posta</label>
                                    <div class="col-sm-5">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="E-Posta" maxlength="200" value="{{ isset($dataUser->email) ? $dataUser->email : old('email') }}" required>
                                        @error('email')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-sm-2 col-form-label">Parola</label>
                                    <div class="col-sm-5">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Parola" maxlength="200" {{ isset($dataUser->id) ? '' : 'required' }} value="{{ old('password') }}">
                                        @error('password')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="admin" class="col-sm-2 col-form-label">Yönetici</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="admin" value="0">
                                            <input type="checkbox" class="custom-control-input" id="admin" name="admin" value="1" {{ @$dataUser->admin == 1 || old('admin') == 1 ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="admin">Yönetici</label>
                                        </div>
                                        @error('admin')
                                            <span class="error-msg">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="active" class="col-sm-2 col-form-label">Aktif</label>
                                    <div class="col-sm-5">
                                        <div class="custom-control custom-switch">
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" class="custom-control-input" id="active" name="active" value="1" {{ isset($dataUser) && isset($dataUser->getAttributes()['active']) ? ($dataUser->getAttributes()['active'] == 1 ? 'checked' : '') : 'checked' }}>
                                            <label class="custom-control-label" for="active">Aktif</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary float-right">Kaydet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
