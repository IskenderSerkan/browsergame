@extends('admin.inc.master')
@section('pagename', 'Kullanıcılar')

@section('meta')
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/datatables/datatables.min.css') }}">
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="tableuser" class="table table-bordered table-striped datatable" style="width:100%" data-ajax="{{ request()->url() }}">
                                <thead>
                                    <tr class="filter-area">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>
                                            <select class="form-control table-filter">
                                                <option value="">Tümü</option>
                                                @foreach ($filter['admin'] as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th>
                                            <select class="form-control table-filter">
                                                <option value="">Tümü</option>
                                                @foreach ($filter['active'] as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr class="table-head">
                                        <th data-data="id" data-width="50">ID</th>
                                        <th data-data="name">Adı Soyadı</th>
                                        <th data-data="email">E-Posta</th>
                                        <th data-data="admin">Yetki</th>
                                        <th data-data="active">Durumu</th>
                                        <th data-data="created_at">Kayıt Tarihi</th>
                                        <th data-data="login_date">Son Giriş</th>
                                        <th data-data="action" data-orderable="false" data-searchable="false" data-width="100">İşlem</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/admin/plugins/datatables/datatables.min.js') }}"></script>
@endsection
