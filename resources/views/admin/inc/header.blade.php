<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BrowserGame Yönetici Paneli</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.css') }}">
    <link rel="shortcut icon" href="{{ asset('assets/admin/dist/img/logo_ico.svg') }}" type="image/x-icon" />
    <link rel="icon" href="{{ asset('assets/admin/dist/img/logo_ico.svg') }}" type="image/x-icon" />
    @yield('meta')
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/admincustom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/panel.css') }}">
</head>

<body class="hold-transition sidebar-mini dark-mode">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                {{-- <li class="nav-item d-none d-sm-inline-block">
                    <a href="index3.html" class="nav-link">Anasayfa</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">İletişim</a>
                </li> --}}
            </ul>

            <ul class="navbar-nav ml-auto">

                {{-- <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-comments"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Brad Diesel
                                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">Call me whenever you can...</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        John Pierce
                                        <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">I got your message bro</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Nora Silvester
                                        <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">The subject goes here</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                    </div>
                </li> --}}

                {{-- <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" id="changetheme" href="#" role="button">
                        <i class="fas fa-moon"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>

        <aside class="main-sidebar sidebar-dark-primary">
            <a href="{{ route('admin') }}" class="brand-link">
                <img src="{{ asset('assets/admin/dist/img/logo_ico.svg') }}" alt="BrowserGame" class="brand-image">
                <span class="brand-text">
                    <img src="{{ asset('assets/admin/dist/img/logo_text.svg') }}" alt="BrowserGame">
                </span>
            </a>
            <div class="sidebar">
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">

                        <li class="nav-item" data-name="user">
                            <a href="javascript:;" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Kullanıcılar
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('user.create') }}" class="nav-link">
                                        <i class="fas fa-plus-square nav-icon"></i>
                                        <p>Kullanıcı Ekle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('user.index') }}" class="nav-link">
                                        <i class="fas fa-list-alt nav-icon"></i>
                                        <p>Kullanıcıları Listele</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item" data-name="category">
                            <a href="javascript:;" class="nav-link">
                                <i class="nav-icon fas fa-boxes"></i>
                                <p>
                                    Kategoriler
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href=" {{ route('category.create') }}" class="nav-link">
                                        <i class="fas fa-plus-square nav-icon"></i>
                                        <p>Kategori Ekle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('category.index') }}" class="nav-link">
                                        <i class="fas fa-list-alt nav-icon"></i>
                                        <p>Kategorileri Listele</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item" data-name="developer">
                            <a href="javascript:;" class="nav-link">
                                <i class="nav-icon fas fa-code"></i>
                                <p>
                                    Geliştiriciler
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href=" {{ route('developer.create') }}" class="nav-link">
                                        <i class="fas fa-plus-square nav-icon"></i>
                                        <p>Geliştirici Ekle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('developer.index') }}" class="nav-link">
                                        <i class="fas fa-list-alt nav-icon"></i>
                                        <p>Geliştirici Listele</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item" data-name="game">
                            <a href="javascript:;" class="nav-link">
                                <i class="nav-icon fas fa-dice"></i>
                                <p>
                                    Oyunlar
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href=" {{ route('game.create') }}" class="nav-link">
                                        <i class="fas fa-plus-square nav-icon"></i>
                                        <p>Oyun Ekle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('game.index') }}" class="nav-link">
                                        <i class="fas fa-list-alt nav-icon"></i>
                                        <p>Oyunları Listele</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item" data-name="slide">
                            <a href="javascript:;" class="nav-link">
                                <i class="nav-icon fas fa-images"></i>
                                <p>
                                    Slaytlar
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href=" {{ route('slide.create') }}" class="nav-link">
                                        <i class="fas fa-plus-square nav-icon"></i>
                                        <p>Slayt Ekle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('slide.index') }}" class="nav-link">
                                        <i class="fas fa-list-alt nav-icon"></i>
                                        <p>Slayt Listele</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item" data-name="page">
                            <a href="javascript:;" class="nav-link">
                                <i class="nav-icon fas fa-file"></i>
                                <p>
                                    Sayfalar
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href=" {{ route('page.create') }}" class="nav-link">
                                        <i class="fas fa-plus-square nav-icon"></i>
                                        <p>Sayfa Ekle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('page.index') }}" class="nav-link">
                                        <i class="fas fa-list-alt nav-icon"></i>
                                        <p>Sayfa Listele</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </aside>

        <div class="content-wrapper">
            @if (Session::has('message'))
                <div class="alert alert-success status-msg">
                    {!! Session::get('message') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>
            @endif
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">@yield('pagename', 'Sayfa')</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('admin') }}">Ana Sayfa</a></li>
                                <li class="breadcrumb-item active">@yield('pagename', 'Sayfa')</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
