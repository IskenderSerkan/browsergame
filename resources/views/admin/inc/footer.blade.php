</div>
<aside class="control-sidebar control-sidebar-dark">
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>

</div>
<script src="{{ asset('assets/admin/plugins/jquery/jquery.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script>
    window.csrfToken = '<?php echo csrf_token(); ?>';
    var menuSelected = '{{ Request::segment(2) }}';

</script>
@yield('script')
<script src="{{ asset('assets/admin/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('assets/admin/panel.js') }}"></script>
</body>

</html>
