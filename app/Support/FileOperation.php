<?php

namespace App\Support;

use File;
use Intervention\Image\Facades\Image;

class FileOperation
{

    static function uploadImage($image, $fileName, $uploadFolder, $width, $height)
    {
        $imgName = $fileName . '.' . $image->getClientOriginalExtension();
        File::makeDirectory($uploadFolder, 0777, true, true);

        Image::make($image)
            ->fit($width, $height)
            ->save($uploadFolder . $imgName);

        return $imgName;
    }
}
