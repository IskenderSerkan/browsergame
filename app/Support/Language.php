<?php

namespace App\Support;

class Language
{
    static function getLanguages()
    {
        return [
            'tr' => 'Türkçe',
            'en' => 'English',
        ];
    }
}
