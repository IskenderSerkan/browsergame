<?php

namespace App\Support;

use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class Logs
{
    static function addLog($operation, $detail, $result, $showInfo = true)
    {
        $user_id = Auth::user()->id;
        $ip = \Request::ip();

        Log::insert([
            'time' => now(),
            'user_id' => $user_id,
            'ip' => $ip,
            'operation' => $operation,
            'detail' => $detail,
            'result' => $result
        ]);

        if ($showInfo === true) {
            session()->flash('message', $detail);
            session()->flash('status', $result);
        }
    }
}
