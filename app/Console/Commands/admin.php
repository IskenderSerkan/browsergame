<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class admin extends Command
{
    protected $signature = 'admin';

    protected $description = 'Yönetici hesabı oluşturmanızı sağlar.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $name = $this->ask('Adınız ve soyadınız');
        if ($name == '' || $name === null) {
            $this->error('Gecerli bir isim yazmalisiniz!');
            exit();
        }

        $email = $this->ask('Yonetici e-posta adresi');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Gecerli bir e-posta adresi yazmalisiniz!');
            exit();
        }

        $password = $this->ask('Parola');
        if ($password == '' || $password === null) {
            $this->error('Gecerli bir sifre yazmalisiniz!');
            exit();
        }

        try {
            User::create([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
                'admin' => 1,
                'active' => 1
            ]);
            $this->info('Hesap basariyla olusturuldu');
        } catch (\Exception $e) {
            $this->alert('Hata olustu: ' . $e->getMessage());
        }

        //return $email;
    }
}
