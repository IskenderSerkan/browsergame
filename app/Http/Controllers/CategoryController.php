<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Support\Logs;
use App\Models\Category;
use App\Support\FileOperation;
use App\Support\Language;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public $uploadFolder;
    function __construct()
    {
        $this->uploadFolder = config('custom.uploadpath') . 'category/';
    }

    /**
     * CRUD İŞLEMLERİ
     */
    // Listeleme işlemi
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::all();
            return $this->listFormat($data);
        }

        // Filtreleri ilet
        // $filter['active'] = Category::groupBy('active')->pluck('active');
        $filter['active'] = [0 => 'Pasif', 1 => 'Aktif'];
        return view('admin.list_category', compact('filter'));
    }

    // Yeni kategori sayfası göster
    public function create()
    {
        $langs = Language::getLanguages();
        return view('admin.edit_category', compact('langs'));
    }

    // Yeni kategori kayıt
    public function store(Request $request)
    {
        $data = $this->dataControl($request);
        $result = Category::create($data);
        Logs::addLog('Kategori', $result->id . ' nolu kategori eklendi', 1);
        return redirect()->route('category.index');
    }

    // Kategori düzenle sayfası göster
    public function show($id)
    {
        $dataCategory = Category::find($id);
        $langs = Language::getLanguages();
        return view('admin.edit_category', compact('dataCategory', 'langs'));
    }

    public function edit($id)
    {
        //
    }

    // Düzenlenen kategorinin kaydedilmesi
    public function update(Request $request, $id)
    {
        $data = $this->dataControl($request, $id);
        Category::find($id)->update($data);
        Logs::addLog('Kategori', $id . ' nolu kategori düzenlendi', 1);
        return redirect()->route('category.index');
    }

    // Silme işlemi
    public function destroy($id)
    {
        $row = Category::find($id);
        if ($row->image) @unlink($this->uploadFolder . $row->image);
        $row->delete();
        Logs::addLog('Kategori', $id . ' nolu kategori silindi', 1, false);
        return redirect()->back();
    }

    /**
     * DİĞER İŞLEMLER
     */
    //Ekle ve güncelle işleminde gelen veriyi kontrol et, işle
    private function dataControl($request, $id = null)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpg,jpeg,png,svg,gif,webp|max:2048',
            'order' => 'required|numeric'
        ]);

        // Düzenleme işleminde kapatılan dil sekmesi varsa o dilin verisini sil
        if ($id !== null) {
            $langs = Language::getLanguages();
            foreach (array_keys($langs) as $key) {
                is_null($request[$key]) ? (Category::find($id)->translate($key) != null ? Category::find($id)->translate($key)->delete() : '') : '';
            }
        }
        $data = $request->all();

        if ($request->image != null) {
            $data['image'] = FileOperation::uploadImage($request->image, time(), $this->uploadFolder, 400, 400);

            // Eski görseli sil
            if ($id !== null) {
                $oldImage = Category::find($id)->image;
                File::delete($this->uploadFolder . $oldImage);
            }
        }

        return $data;
    }

    public function listFormat($data)
    {
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<div class="btn-group">' .
                    '<a href="' . route('category.show', $row->id) . '" class="btn btn-primary">Düzenle</a>' .
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">' .
                    '<span class="sr-only">Toggle Dropdown</span>' .
                    '</button>' .
                    '<div class="dropdown-menu" role="menu">' .
                    '<a class="dropdown-item" href="' . route('category.show', $row->id) . '">Düzenle</a>' .
                    '<a class="dropdown-item list-delete" href="' . route('category.destroy', $row->id) . '" data-confirm="Kategori ve bağlı tüm içerikler silinecek! Devam edilsin mi?">Sil</a>' .
                    '</div>' .
                    '</div>';
                return $btn;
            })
            ->editColumn('image', function ($value) {
                return $value->image != null ? '<img class="table-img" src="' . url('uploads/category/' . $value->image) . '"/>' : 'Görsel yok';
            })
            ->editColumn('active', function ($value) {
                $status = $value->active == 'Aktif' ? 'success' : 'danger';
                return '<span class="badge badge-' . $status . '">' . $value->active . '</span>';
            })
            ->rawColumns(['active', 'action', 'image'])
            ->make(true);
    }
}
