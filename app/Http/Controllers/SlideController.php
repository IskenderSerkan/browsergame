<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Models\Slide;
use App\Support\Logs;
use App\Support\Language;
use Illuminate\Http\Request;
use App\Support\FileOperation;

class SlideController extends Controller
{
    public $uploadFolder;
    function __construct()
    {
        $this->uploadFolder = config('custom.uploadpath') . 'slide/';
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Slide::all();
            return $this->listFormat($data);
        }

        $filter['active'] = [0 => 'Pasif', 1 => 'Aktif'];
        return view('admin.list_slide', compact('filter'));
    }

    public function create()
    {
        $langs = Language::getLanguages();
        return view('admin.edit_slide', compact('langs'));
    }

    public function store(Request $request)
    {
        $data = $this->dataControl($request);
        $result = Slide::create($data);
        Logs::addLog('Slayt', $result->id . ' nolu slayt eklendi', 1);
        return redirect()->route('slide.index');
    }

    public function show($id)
    {
        $dataSlide = Slide::find($id);
        $langs = Language::getLanguages();
        return view('admin.edit_slide', compact('dataSlide', 'langs'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->dataControl($request, $id);
        Slide::find($id)->update($data);
        Logs::addLog('Slayt', $id . ' nolu slayt düzenlendi', 1);
        return redirect()->route('slide.index');
    }

    public function destroy($id)
    {
        $row = Slide::find($id);
        if ($row->desktop_img) @unlink($this->uploadFolder . $row->desktop_img);
        if ($row->mobile_img) @unlink($this->uploadFolder . $row->mobile_img);
        $row->delete();
        Logs::addLog('Slayt', $id . ' nolu slayt silindi', 1, false);
        return redirect()->back();
    }

    private function dataControl($request, $id = null)
    {
        $this->validate($request, [
            'desktop_img' => ($id === null ? 'required|' : '') . 'image|mimes:jpg,jpeg,png,svg,gif,webp|max:2048',
            'mobile_img' => ($id === null ? 'required|' : '') . 'image|mimes:jpg,jpeg,png,svg,gif,webp|max:2048',
            'order' => 'required|numeric'
        ]);

        // Düzenleme işleminde kapatılan dil sekmesi varsa o dilin verisini sil
        if ($id !== null) {
            $langs = Language::getLanguages();
            foreach (array_keys($langs) as $key) {
                is_null($request[$key]) ? (Slide::find($id)->translate($key) != null ? Slide::find($id)->translate($key)->delete() : '') : '';
            }
        }
        $data = $request->all();

        $oldImages = Slide::select('desktop_img', 'mobile_img')->find($id);

        if ($request->desktop_img != null) {
            $data['desktop_img'] = FileOperation::uploadImage($request->desktop_img, time(), $this->uploadFolder, 1160, 600);
            $id !== null ? File::delete($this->uploadFolder . $oldImages->desktop_img) : '';
        }
        if ($request->mobile_img != null) {
            $data['mobile_img'] = FileOperation::uploadImage($request->mobile_img, time() . 'm', $this->uploadFolder, 450, 400);
            $id !== null ? File::delete($this->uploadFolder . $oldImages->mobile_img) : '';
        }

        return $data;
    }

    public function listFormat($data)
    {
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<div class="btn-group">' .
                    '<a href="' . route('slide.show', $row->id) . '" class="btn btn-primary">Düzenle</a>' .
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">' .
                    '<span class="sr-only">Toggle Dropdown</span>' .
                    '</button>' .
                    '<div class="dropdown-menu" role="menu">' .
                    '<a class="dropdown-item" href="' . route('slide.show', $row->id) . '">Düzenle</a>' .
                    '<a class="dropdown-item list-delete" href="' . route('slide.destroy', $row->id) . '" data-confirm="Slayt içeriği silinecek! Devam edilsin mi?">Sil</a>' .
                    '</div>' .
                    '</div>';
                return $btn;
            })
            ->editColumn('desktop_img', function ($value) {
                return $value->desktop_img != null ? '<img class="table-img" src="' . url('uploads/slide/' . $value->desktop_img) . '"/>' : 'Görsel yok';
            })
            ->editColumn('active', function ($value) {
                $status = $value->active == 'Aktif' ? 'success' : 'danger';
                return '<span class="badge badge-' . $status . '">' . $value->active . '</span>';
            })
            ->rawColumns(['active', 'action', 'desktop_img'])
            ->make(true);
    }
}
