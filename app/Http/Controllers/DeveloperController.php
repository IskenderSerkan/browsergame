<?php

namespace App\Http\Controllers;

use File;
use DataTables;
use App\Support\Logs;
use App\Models\Developer;
use App\Support\FileOperation;
use Illuminate\Http\Request;

class DeveloperController extends Controller
{

    public $uploadFolder;
    function __construct()
    {
        $this->uploadFolder = config('custom.uploadpath') . 'developer/';
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Developer::all();
            return $this->listFormat($data);
        }

        $filter['active'] = [0 => 'Pasif', 1 => 'Aktif'];
        return view('admin.list_developer', compact('filter'));
    }

    public function create()
    {
        return view('admin.edit_developer');
    }


    public function store(Request $request)
    {
        $data = $this->dataControl($request);
        $result = Developer::create($data);
        Logs::addLog('Geliştirici', $result->id . ' nolu geliştirici eklendi', 1);
        return redirect()->route('developer.index');
    }

    public function show($id)
    {
        $dataDeveloper = Developer::find($id);
        return view('admin.edit_developer', compact('dataDeveloper'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->dataControl($request, $id);
        Developer::find($id)->update($data);
        Logs::addLog('Geliştirici', $id . ' nolu geliştirici düzenlendi', 1);
        return redirect()->route('developer.index');
    }

    public function destroy($id)
    {
        $row = Developer::find($id);
        if ($row->logo) @unlink($this->uploadFolder . $row->logo);
        $row->delete();
        Logs::addLog('Geliştirici', $id . ' nolu geliştirici silindi', 1, false);
        return redirect()->back();
    }

    private function dataControl($request, $id = null)
    {
        $this->validate($request, [
            'name' => 'required',
            'logo' => 'image|mimes:jpg,jpeg,png,svg,gif,webp|max:2048',
            'order' => 'required|numeric'
        ]);

        $data = $request->all();

        if ($request->logo != null) {
            $data['logo'] = FileOperation::uploadImage($request->logo, time(), $this->uploadFolder, 300, 300);

            if ($id !== null) {
                $oldLogo = Developer::find($id)->logo;
                File::delete($this->uploadFolder . $oldLogo);
            }
        }

        return $data;
    }

    public function listFormat($data)
    {
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<div class="btn-group">' .
                    '<a href="' . route('developer.show', $row->id) . '" class="btn btn-primary">Düzenle</a>' .
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">' .
                    '<span class="sr-only">Toggle Dropdown</span>' .
                    '</button>' .
                    '<div class="dropdown-menu" role="menu">' .
                    '<a class="dropdown-item" href="' . route('developer.show', $row->id) . '">Düzenle</a>' .
                    '<a class="dropdown-item list-delete" href="' . route('developer.destroy', $row->id) . '" data-confirm="Geliştirici ve kendisine bağlı tüm oyunlar silinecek! Devam edilsin mi?">Sil</a>' .
                    '</div>' .
                    '</div>';
                return $btn;
            })
            ->editColumn('logo', function ($value) {
                return $value->logo != null ? '<img class="table-img" src="' . url('uploads/developer/' . $value->logo) . '"/>' : 'Görsel yok';
            })
            ->editColumn('active', function ($value) {
                $status = $value->active == 'Aktif' ? 'success' : 'danger';
                return '<span class="badge badge-' . $status . '">' . $value->active . '</span>';
            })
            ->rawColumns(['active', 'action', 'logo'])
            ->make(true);
    }
}
