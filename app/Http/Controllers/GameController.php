<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Game;
use App\Models\User;
use App\Support\Logs;
use App\Models\Category;
use App\Models\Developer;
use App\Support\Language;
use Illuminate\Http\Request;
use App\Support\FileOperation;
use Illuminate\Support\Facades\Auth;
use File;

class GameController extends Controller
{
    public $uploadFolder;
    function __construct()
    {
        $this->uploadFolder = config('custom.uploadpath') . 'game/';
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Game::all();
            return $this->listFormat($data);
        }

        $filter['categories'] = Category::all()->pluck('name');
        $filter['developers'] = Developer::all()->pluck('name');
        $filter['admins'] = User::where('admin', 1)->pluck('name');
        $filter['active'] = [0 => 'Pasif', 1 => 'Aktif'];
        return view('admin.list_game', compact('filter'));
    }

    public function create()
    {
        $developers = Developer::pluck('name', 'id');
        $categories = Category::select('id')->get();
        $langs = Language::getLanguages();
        return view('admin.edit_game', compact('langs', 'developers', 'categories'));
    }

    public function store(Request $request)
    {
        $data = $this->dataControl($request);
        $result = Game::create($data);

        // game_categories pivot tablosunu güncellemek için (Oyunun bağlı olduğu kategoriler)
        $categoryIDs = [];
        foreach ($request->categories as $categoryID) {
            $categoryIDs[] = ['categories_id' => $categoryID];
        }
        Game::find($result->id)->getCategories()->sync($categoryIDs);

        Logs::addLog('Oyun', $result->id . ' nolu oyun eklendi', 1);
        return redirect()->route('game.index');
    }

    public function show($id)
    {
        $dataGame = Game::find($id);
        $developers = Developer::pluck('name', 'id');
        $categories = Category::select('id')->get();
        $selectedCategories = Game::find($id)->getCategories()->get()->pluck('id')->toArray();
        $langs = Language::getLanguages();
        return view('admin.edit_game', compact('dataGame', 'langs', 'developers', 'categories', 'selectedCategories'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->dataControl($request, $id);

        Game::find($id)->update($data);

        // game_categories pivot tablosunu güncellemek için (Oyunun bağlı olduğu kategoriler)
        $categoryIDs = [];
        foreach ($request->categories as $categoryID) {
            $categoryIDs[] = ['categories_id' => $categoryID];
        }
        Game::find($id)->getCategories()->sync($categoryIDs);

        Logs::addLog('Oyun', $id . ' nolu oyun düzenlendi', 1);
        return redirect()->route('game.index');
    }

    public function destroy($id)
    {
        $row = Game::find($id);
        if ($row->poster) @unlink($this->uploadFolder . $row->poster);
        $row->delete();
        Logs::addLog('Oyun', $id . ' nolu oyun silindi', 1, false);
        return redirect()->back();
    }

    private function dataControl($request, $id = null)
    {
        $this->validate($request, [
            'poster' => ($id === null ? 'required|' : '') . 'image|mimes:jpg,jpeg,png,svg,gif,webp|max:2048',
            'embed' => 'required',
            'categories' => 'required'
        ]);

        // Düzenleme işleminde kapatılan dil sekmesi varsa o dilin verisini sil
        if ($id !== null) {
            $langs = Language::getLanguages();
            foreach (array_keys($langs) as $key) {
                is_null($request[$key]) ? (Game::find($id)->translate($key) != null ? Game::find($id)->translate($key)->delete() : '') : '';
            }
        }
        $data = $request->all();
        $data['adding_by'] = Auth::user()->id;

        if ($request->poster != null) {
            $data['poster'] = FileOperation::uploadImage($request->poster, time(), $this->uploadFolder, 240, 340);
            FileOperation::uploadImage($request->poster, time() . '-small', $this->uploadFolder, 60, 85);

            if ($id !== null) {
                $oldFile = Game::find($id)->poster;
                File::delete($this->uploadFolder . $oldFile);
                // Eski resmi sil
                $oldFileExt = pathinfo($oldFile, PATHINFO_EXTENSION);
                $oldFileSmall = basename($oldFile, '.' . $oldFileExt) . '-small.' . $oldFileExt;
                File::delete($this->uploadFolder . $oldFileSmall);
            }
        }

        return $data;
    }

    public function listFormat($data)
    {
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($value) {
                $btn = '<div class="btn-group">' .
                    '<a href="' . route('game.show', $value->id) . '" class="btn btn-primary">Düzenle</a>' .
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">' .
                    '<span class="sr-only">Toggle Dropdown</span>' .
                    '</button>' .
                    '<div class="dropdown-menu" role="menu">' .
                    '<a class="dropdown-item" href="' . route('game.show', $value->id) . '">Düzenle</a>' .
                    '<a class="dropdown-item list-delete" href="' . route('game.destroy', $value->id) . '" data-confirm="Oyun silinsin mi?">Sil</a>' .
                    '</div>' .
                    '</div>';
                return $btn;
            })
            ->editColumn('poster', function ($value) {
                return $value->poster != null ? '<img class="table-img" src="' . url('uploads/game/' . $value->poster) . '"/>' : 'Görsel yok';
            })
            ->editColumn('developer', function ($value) {

                return $value->getDeveloper != null ? $value->getDeveloper->name : '';
            })
            ->addColumn('categories', function ($value) {
                $category = $value->getCategories->toArray();

                $categories = array_map(function ($array) {
                    return '<span class="badge badge-info">' . $array['name'] . '</span>';
                }, $category);

                return implode(' ', $categories);
            })
            ->addColumn('addingby', function ($value) {
                return $value->getAddingBy->name;
            })
            ->editColumn('active', function ($value) {
                $status = $value->active == 'Aktif' ? 'success' : 'danger';
                return '<span class="badge badge-' . $status . '">' . $value->active . '</span>';
            })
            ->rawColumns(['active', 'action', 'poster', 'categories'])
            ->make(true);
    }
}
