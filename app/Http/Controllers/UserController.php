<?php

namespace App\Http\Controllers;

use DataTables;
use Carbon\Carbon;
use App\Models\User;
use App\Support\Logs;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::all();
            return $this->listFormat($data);
        }

        $filter['admin'] = [0 => 'Kullanıcı', 1 => 'Yönetici'];
        $filter['active'] = [0 => 'Pasif', 1 => 'Aktif'];
        return view('admin.list_user', compact('filter'));
    }

    public function create()
    {
        return view('admin.edit_user');
    }

    public function store(Request $request)
    {
        $data = $this->dataControl($request);
        $result = User::create($data);
        Logs::addLog('Kullanıcı', $result->id . ' nolu kullanıcı eklendi', 1);
        return redirect()->route('user.index');
    }

    public function show($id)
    {
        $dataUser = User::find($id);
        return view('admin.edit_user', compact('dataUser'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->dataControl($request, $id);
        User::find($id)->update($data);
        Logs::addLog('Kullanıcı', $id . ' nolu kullanıcı düzenlendi', 1);
        return redirect()->route('user.index');
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        Logs::addLog('Kullanıcı', $id . ' nolu kullanıcı silindi', 1, false);
        return redirect()->back();
    }

    //DİĞER İŞLEMLER
    private function dataControl($request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:200',
            'email' => 'required|email',
            'password' => !empty($request->password) ? 'min:6|max:200' : '',
            'admin' => 'boolean'
        ]);

        $data = $request->all();
        return $data;
    }

    public function listFormat($data)
    {
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<div class="btn-group">' .
                    '<a href="' . route('user.show', $row->id) . '" class="btn btn-primary">Düzenle</a>' .
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">' .
                    '<span class="sr-only">Toggle Dropdown</span>' .
                    '</button>' .
                    '<div class="dropdown-menu" role="menu">' .
                    '<a class="dropdown-item" href="' . route('user.show', $row->id) . '">Düzenle</a>' .
                    '<a class="dropdown-item list-delete" href="' . route('user.destroy', $row->id) . '" data-confirm="Kategori ve bağlı tüm içerikler silinecek! Devam edilsin mi?">Sil</a>' .
                    '</div>' .
                    '</div>';
                return $btn;
            })
            ->editColumn('created_at', function ($value) {
                return date('d.m.Y H:i:s', strtotime($value->created_at));
            })
            ->editColumn('login_date', function ($value) {
                if ($value->login_date != '') {
                    return Carbon::parse($value->login_date)->diffForHumans();
                } else {
                    return '';
                }
            })
            ->editColumn('active', function ($value) {
                $status = $value->active == 'Aktif' ? 'success' : 'danger';
                return '<span class="badge badge-' . $status . '">' . $value->active . '</span>';
            })
            ->editColumn('admin', function ($value) {
                return $value->admin == 1 ? 'Yönetici' : 'Kullanıcı';
            })
            ->rawColumns(['admin', 'action', 'active'])
            ->make(true);
    }
}
