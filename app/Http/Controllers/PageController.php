<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Page;
use App\Support\Logs;
use App\Support\Language;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Page::select('id', 'order', 'footer', 'submenu', 'active')->get();
            return $this->listFormat($data);
        }

        $filter['active'] = [0 => 'Pasif', 1 => 'Aktif'];
        return view('admin.list_page', compact('filter'));
    }

    public function create()
    {
        $langs = Language::getLanguages();
        return view('admin.edit_page', compact('langs'));
    }

    public function store(Request $request)
    {
        $data = $this->dataControl($request);
        $result = Page::create($data);
        Logs::addLog('Sayfa', $result->id . ' nolu sayfa eklendi', 1);
        return redirect()->route('page.index');
    }

    public function show($id)
    {
        $dataPage = Page::find($id);
        $langs = Language::getLanguages();
        return view('admin.edit_page', compact('dataPage', 'langs'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->dataControl($request, $id);
        Page::find($id)->update($data);
        Logs::addLog('Sayfa', $id . ' nolu sayfa düzenlendi', 1);
        return redirect()->route('page.index');
    }

    public function destroy($id)
    {
        Page::find($id)->delete();
        Logs::addLog('Sayfa', $id . ' nolu sayfa silindi', 1, false);
        return redirect()->back();
    }

    private function dataControl($request, $id = null)
    {
        $this->validate($request, [
            'order' => 'required|numeric'
        ]);

        if ($id !== null) {
            $langs = Language::getLanguages();
            foreach (array_keys($langs) as $key) {
                is_null($request[$key]) ? (Page::find($id)->translate($key) != null ? Page::find($id)->translate($key)->delete() : '') : '';
            }
        }
        $data = $request->all();

        return $data;
    }

    public function listFormat($data)
    {
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<div class="btn-group">' .
                    '<a href="' . route('page.show', $row->id) . '" class="btn btn-primary">Düzenle</a>' .
                    '<button type="button" class="btn btn-primary dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">' .
                    '<span class="sr-only">Toggle Dropdown</span>' .
                    '</button>' .
                    '<div class="dropdown-menu" role="menu">' .
                    '<a class="dropdown-item" href="' . route('page.show', $row->id) . '">Düzenle</a>' .
                    '<a class="dropdown-item list-delete" href="' . route('page.destroy', $row->id) . '" data-confirm="Sayfa silinecek! Devam edilsin mi?">Sil</a>' .
                    '</div>' .
                    '</div>';
                return $btn;
            })
            ->editColumn('active', function ($value) {
                $status = $value->active == 'Aktif' ? 'success' : 'danger';
                return '<span class="badge badge-' . $status . '">' . $value->active . '</span>';
            })
            ->rawColumns(['active', 'action'])
            ->make(true);
    }
}
