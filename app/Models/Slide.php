<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Slide extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['desktop_img', 'mobile_img', 'link', 'order', 'active'];
    public $translatedAttributes = ['title', 'description', 'button'];

    function getActiveAttribute($value)
    {
        return $value == 1 ? 'Aktif' : 'Pasif';
    }
}
