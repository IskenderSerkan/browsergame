<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Page extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['order', 'footer', 'submenu', 'active'];
    public $translatedAttributes = ['title', 'content'];

    public function getActiveAttribute($value)
    {
        return $value == 1 ? 'Aktif' : 'Pasif';
    }
}
