<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use App\Models\Developer;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Game extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['poster', 'embed', 'developer_id', 'mobile', 'click', 'release_date', 'active', 'adding_by'];
    public $translatedAttributes = ['name', 'description'];

    public function getActiveAttribute($value)
    {
        return $value == 1 ? 'Aktif' : 'Pasif';
    }

    public function getCategories()
    {
        return $this->belongsToMany(Category::class, 'game_categories', 'game_id', 'categories_id');
    }

    public function getDeveloper()
    {
        return $this->belongsTo(Developer::class, 'developer_id', 'id');
    }

    public function getAddingBy()
    {
        return $this->belongsTo(User::class, 'adding_by', 'id');
    }
}
