<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SlideTranslation extends Model
{
    use HasFactory;

    protected $table = 'slides_langs';
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'button'];
}
