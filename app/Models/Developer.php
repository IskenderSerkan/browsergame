<?php

namespace App\Models;

use App\Models\Game;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Developer extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'logo',
        'order',
        'active'
    ];

    public function getActiveAttribute($value)
    {
        return $value != 1 ? 'Pasif' : 'Aktif';
    }

    public function getGames()
    {
        return $this->hasMany(Game::class, 'developer_id', 'id');
    }
}
