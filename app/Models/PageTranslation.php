<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    use HasFactory;
    protected $table = 'pages_langs';
    public $timestamps = false;
    protected $fillable = ['title', 'content'];
}
