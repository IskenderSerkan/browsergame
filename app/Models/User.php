<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'admin',
        'login_date',
        'active'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getActiveAttribute($value)
    {
        return $value != 1 ? 'Pasif' : 'Aktif';
    }

    public function ratings()
    {
        return $this->belongsToMany(Game::class, 'game_rating', 'user_id', 'game_id')->withPivot('rating')->withTimestamps();
    }
}
