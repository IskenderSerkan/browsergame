<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Category extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['image', 'order', 'active'];
    public $translatedAttributes = ['name', 'description'];

    public function getActiveAttribute($value)
    {
        return $value == 1 ? 'Aktif' : 'Pasif';
    }
}
