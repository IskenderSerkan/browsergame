<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GameTranslation extends Model
{
    use HasFactory;

    protected $table = 'games_langs';
    public $timestamps = false;
    protected $fillable = ['name', 'description'];
}
