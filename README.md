Laravel 8 kullanılarak yazılmıştır.

**Kullanılan işlemler:**

- Veritabanı tablolarının migration işlemi ile oluşturulması
- Migration işlemlerinden foreign kullanarak tabloların birbirleriyle ilişkilerinin tanımlanması
- Factory ile veri üreterek Seed işlemi ile user tablosuna örnek 100 kullanıcı verisinin girilmesi
- Route resource ile CRUD işlerimleri
- Middleware işlemi ile panel sayfalarına yalnızca yönetici yetkili kişilerin girebilmesi
- Tablolar için model (Eloquent ORM) tanımlanarak yapılacak işlemlerin controller üzerinden viewe gönderlmesi
- View dosyalarında yield, section, extends kullanarak sayfa şablonlarının oluşturulması
- Yajra Datatable üzerine ek olarak filtrelerin eklenmesi
- Model dosyaları üzerinden tablo ilişikilerinin kurulması (Eloquent ORM Relationship ile hasOne, belongsTo, belongsToMany) 
- Many to Many Pivot ile ara tabloların ilişkilendirilerek kullanılması
- Eloquent ORM Model ile veritabanından gelen verinin biçimlendirilmiş olarak getirilmesi
- Form işlemleri (csrf ile koruma, validate ile veri kontrol...)
- Session flash data ile bildirim alanına bilgi iletilmesi
- Carbon ile tarih saat biçimlendirmesi
- Custom command ile özel terminal komutu (terminalden yönetici hesabı oluşturulmasını sağlar)





# Kullanılan Kütüphaneler

**Laravel Translatable**

Laravelde dil desteği bağlantılarını kurmayı kolaylaştıran, Laravel blogunda da önerilen dil desteği kütüphanesi
https://github.com/Astrotomic/laravel-translatable


**Yajra Datatable**

Datatable'ın Laravel'e uygun hale getirilmiş kütüphanesi
https://github.com/yajra/laravel-datatables


**Intervention Image**

Görsellere ebatlama, kırma ve efektler vermeyi sağlayan resim işleme kütüphanesi
http://image.intervention.io

**Admin LTE 3**

Ücretsiz yönetici paneli şablonu bazı görüntüsüne bazı ufak müdaleler eklenerek kullanıldı.
https://adminlte.io


# Kurulum
- Git clone [adres]
- composer install
- .env.example dosyası üzerinden .env dosyasını oluşturun veritabanı bilgilerini girin
- php artisan key:generate - Laravel anahtarını bilgisayara göre güncelleme
- php artisan migrate - Vertabanı tablolarını oluşturma
- php artisan db:seed - Örnek kullanıcı verisi oluşturup veritabanına girme
- php artisan admin - Yönetici hesabı oluşturma
