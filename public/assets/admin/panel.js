//Datatable varsayılan ayarlar
if (typeof jQuery.fn.dataTable !== 'undefined') {
    (function($, DataTable) {
        $.extend(true, DataTable.defaults, {
            language: {
                "sProcessing": "İşleniyor...",
                "sLengthMenu": "_MENU_ kayıt göster",
                "sZeroRecords": "Sonuç bulunamadı",
                "sEmptyTable": "Bu tabloda veri yok",
                "sInfo": "_TOTAL_ kayıttan _START_ ile _END_ arası gösteriliyor",
                "sInfoEmpty": "Görüntülenecek kayıt yok",
                "sInfoFiltered": "(_MAX_ kayıt filtreleniyor)",
                "sInfoPostFix": "",
                "sSearch": "Ara:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Yükleniyor...",
                "oPaginate": {
                    "sFirst": "<<",
                    "sLast": ">>",
                    "sNext": ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending": "Sütunu artan düzende sırala",
                    "sSortDescending": "Sütunu azalan düzende sırala"
                }
            }
        });
    })(jQuery, jQuery.fn.dataTable);
}

$(function() {
    // Datatable'ları oluştur
    $('.datatable').each(function() {
        $(this).DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            stateSave: true,
            initComplete: function() {
                // Filtelerde seçim varsa localStorage içinden oku ve oluşturulunca seçili hale getir
                var tableID = $(this).attr('id');
                var localStorageAddress = 'DataTables_' + tableID + '_' + window.location.pathname;
                var data = JSON.parse(localStorage.getItem(localStorageAddress));
                var columnData = data['columns'];
                var tableObj = $('#' + tableID);

                this.api().columns().every(function() {
                    var searchVal = columnData[this.index()]['search']['search'];
                    tableObj.find('.filter-area th').eq(this.index()).find('select.table-filter').val(searchVal).trigger('change');
                });

            }
        });
    });

    // Select2'leri oluştur
    if ($('.select2').length > 0) {
        $('.select2').select2({
            language: 'tr'
        });
    }

    // Summernote Editörleri yükle
    if ($('.summernote').length > 0) {
        $('.summernote').summernote({
            lang: 'tr-TR',
            height: 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
    }

    langTabControl();
});

// Menüde ilgili alanı aktif hale getir
$('.sidebar .nav-item[data-name="' + menuSelected + '"]').addClass('menu-open');

$('#changetheme').on('click', function(e) {
    e.preventDefault();
    $('body').toggleClass('dark-mode');

    if ($('body').hasClass('dark-mode')) {
        $('.main-header').addClass('navbar-dark').removeClass('navbar-light');
    } else {
        $('.main-header').toggleClass('navbar-light').removeClass('navbar-dark');;
    }

    return false;
});

// Düzenleme ekranlarında dil sekmelerinin aktif/pasif durumu
$('#langtabs .langselect label').on('click', function() {
    var obj = $('#' + $(this).attr('for'));
    obj.prop('checked', !obj.prop('checked'));
    langTabControl();
    return false;
});

function langTabControl() {
    $('#langtabs input[type=checkbox]').each(function() {
        var langArea = $($(this).closest('.nav-link').attr('href'));

        if ($(this).is(':checked')) {
            langArea.find('input, textarea, button').prop('disabled', false);
            $('.summernote').length > 0 ? langArea.find('.summernote').summernote('enable') : '';
        } else {
            langArea.find('input, textarea, button').prop('disabled', true);
            $('.summernote').length > 0 ? langArea.find('.summernote').summernote('disable') : '';
        }
    });

}
//----------------------------------------------------------

// Ajax kayıt silme işlemi
$('body').on('click', '.list-delete', function(e) {
    e.preventDefault();
    var confirmMsg = $(this).attr('data-confirm') != undefined ? $(this).attr('data-confirm') : 'İçeriği silmek istediğinizden emin misiniz?';
    var url = $(this).attr('href');
    var table = $('#' + $(this).closest('.dataTable').attr('id'));
    if (confirm(confirmMsg)) {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                "_token": window.csrfToken,
                "_method": "delete"
            },
            success: function(res) {
                table.DataTable().ajax.reload();
            }
        });
    }
    return false;
});


// Input File için resim önizleme
function previewImage(object) {
    var previewObj = $(object).closest('.form-group').find('.preview-img');

    if (object.files && object.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            previewObj.attr('src', e.target.result);
        }
        reader.readAsDataURL(object.files[0]);
    }
}

// Datatable listeleme işleminde filtreler
$('.table-filter').on('change', function() {
    var tableID = $('#' + $(this).closest('.dataTable').attr('id'));
    var filterValue = $(this).val();
    var filterColumn = $(this).closest('th').index();
    tableID.DataTable().columns(filterColumn).search(filterValue).draw();
    $(this).css('opacity', $(this).val() == '' ? '0.5' : 1);
});
