<?php

use App\Models\Game;
use App\Http\Middleware\CheckAdmin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SlideController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DeveloperController;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// Panel
Route::middleware([CheckAdmin::class])->group(function () {

    Route::prefix('admin')->group(function () {
        Route::view('/', 'admin.index')->name('admin');

        Route::resource('user', UserController::class);
        Route::resource('category', CategoryController::class);
        Route::resource('developer', DeveloperController::class);
        Route::resource('game', GameController::class);
        Route::resource('slide', SlideController::class);
        Route::resource('page', PageController::class);
    });
});


Route::get('/test', function () {
    //dump(Game::find(1)->getCategories()->get()->pluck('id'));

    //return Auth::user()->id;
    /*
    $oyun = Game::find(1);
    dd($oyun->getDeveloper->name);
    */
    /*
    $kategori = Developer::find(2);
    $oyunlar = $kategori->getGames;
    dd($oyunlar);
*/
    // foreach ($kategoriler as $value) {
    //     dump($value->name);
    // }

    //return Hash::make('123456');

    // return \Request::ip();

    // $kategori = new Category;
    // $sonuc = $kategori::find(2)->delete();
    // $sonuc = $kategori::find(2)->getLanguage('tr')->update([
    //     'name'=>'Kategori yen isim',
    //     'description'=>'açıklama burada yer alsın test amaçlı giriş'
    // ]);
    // return $sonuc;
});
